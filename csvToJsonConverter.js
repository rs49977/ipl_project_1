const csv = require("csvtojson")
const fs = require('fs')
const path = require('path')

// convert csvfile to json file 
function csvToJsonConverter (csvFile,jsonFile){
    const filePath = path.resolve(__dirname,csvFile);
    csv().fromFile(filePath).then((jsonData) => {
        jsonToFileConvertor(jsonData,jsonFile)
    })
}

// write data into json file  
function jsonToFileConvertor (jsonData,jsonFile){
    const filePath = path.resolve(__dirname,jsonFile);
    fs.writeFile(filePath,JSON.stringify(jsonData),(err) =>{
        console.log(err);
    })
}

// main function that invoke above functions
function startCsvToJsonConverter() {
    csvToJsonConverter('./src/data/matches.csv','./src/data/matches.json')
    csvToJsonConverter('./src/data/deliveries.csv','./src/data/deliveries.json')
}

// invoking main function
startCsvToJsonConverter();