const mysql = require('mysql')
const dotenv = require('dotenv')

const result = dotenv.config()
if (result.error) console.log(result.parsed)
   
const conn = mysql.createConnection({
    host: process.env.DATABASE_HOST, 
    user: process.env.DATABASE_USER, 
    password: process.env.DATABASE_PASS,
    database: process.env.DATABASE_NAME
})

function executeQry(sqlServer, sqlQuery) {
    return sqlServer.query(sqlQuery, (err, result) => {
        if(err) throw err
        else {
            // console.log('executed successfully !!')
            // return result
            console.log(result)
        }
    })
}


try {
    conn.connect(err => {
        if(err) throw err
        else {
            console.log('connected')

            const allMatchesPerSeasonQry = `SELECT season, COUNT(*) AS matches 
            FROM matches  
            GROUP BY season;`
            // executeQry(conn, allMatchesPerSeasonQry)
            
            const teamMatchesWinPerSeasonQry = `SELECT  matches.season, teams.team_name, count(winner) AS total_wins
            FROM matches 
            JOIN teams ON teams.team_id = matches.winner
            GROUP BY season, winner
            ORDER BY season;`
            // executeQry(conn, teamMatchesWinPerSeasonQry)

            const perTeamExtraRuns2016 = `SELECT teams.team_name, SUM(deliveries.extra_runs) AS total_extra
            FROM deliveries
            JOIN matches ON matches.id = deliveries.match_id
            JOIN teams ON teams.team_id = deliveries.bowling_team
            WHERE matches.season = 2016
            GROUP BY deliveries.bowling_team;`
            // executeQry(conn, perTeamExtraRuns2016)

            const topTenEconomyBowler2015 = `SELECT players.player_name AS bowler, 
            SUM(total_runs)/(COUNT(total_runs)/6) AS economy
            FROM deliveries
            JOIN matches ON matches.id = deliveries.match_id
            JOIN players ON players.player_id = deliveries.bowler
            WHERE matches.season = 2015
            GROUP BY deliveries.bowler
            ORDER BY economy LIMIT 10;`
            // executeQry(conn, topTenEconomyBowler2015)

            const tossAndMatchWinnerTeam = `SELECT teams.team_name, 
            COUNT(matches.winner) toss_and_match_wins
            FROM matches
            JOIN teams ON matches.winner = teams.team_id
            where matches.toss_winner = matches.winner
            GROUP BY matches.winner;`
            // executeQry(conn, tossAndMatchWinnerTeam)

            const topTenEconomyBowlerSuperOver = `SELECT players.player_name, 
            SUM(total_runs)/(COUNT(total_runs)/6) AS economy_super_over
            FROM deliveries
            JOIN players ON players.player_id = deliveries.bowler
            WHERE deliveries.is_super_over = 1
            GROUP BY deliveries.bowler
            ORDER BY economy LIMIT 10;`
            // executeQry(conn, topTenEconomyBowlerSuperOver)

            const abDeVillersStrikeRatePerSeason = `SELECT matches.season, players.player_name, 
            SUM(deliveries.batsman_runs)/COUNT(deliveries.batsman)*100 AS strike_rate
            FROM deliveries
            JOIN matches ON matches.id = deliveries.match_id
            JOIN players ON players.player_id = deliveries.batsman
            WHERE players.player_name LIKE "AB de Villiers%"
            GROUP BY matches.season, deliveries.batsman;`
            // executeQry(conn, abDeVillersStrikeRatePerSeason)

            const bowlerBatsmanHighestDismissal = `SELECT p1.player_name AS bowler, p2.player_name AS batsman,
            COUNT(deliveries.player_dismissed) AS dismissed
           FROM deliveries
           JOIN players p1 ON p1.player_id = deliveries.bowler
           JOIN players p2 ON p2.player_id = deliveries.player_dismissed
           WHERE deliveries.player_dismissed IS NOT NULL 
           AND deliveries.dismissal_kind NOT IN('retired hurt', 'run out', 'obstructing the field')
           GROUP BY deliveries.bowler, deliveries.player_dismissed
           ORDER BY dismissed DESC
           LIMIT 1;`
        //    executeQry(conn, bowlerBatsmanHighestDismissal)

           const perSeasonHighestManOfTheMatch = `SELECT matches.season, 
           players.player_name as pp, COUNT(matches.player_of_match) AS times_man_of_the_match
           FROM matches 
           JOIN players ON players.player_id = matches.player_of_match 
           GROUP BY matches.season, matches.player_of_match
           ORDER BY times_man_of_the_match DESC`
        //    executeQry(conn, perSeasonHighestManOfTheMatch)

           const rcbNoBalls2015 = `SELECT teams.team_name AS team,
           SUM(deliveries.noball_runs) AS no_balls
          FROM deliveries
          JOIN teams ON teams.team_id = deliveries.bowling_team
          JOIN matches ON matches.id = deliveries.match_id
          WHERE matches.season = 2015 AND teams.team_name = "Royal Challengers Bangalore";`
          executeQry(conn, rcbNoBalls2015)

          const dhoniBoundryRuns2015 = `SELECT players.player_name AS batsman,
          SUM(deliveries.batsman_runs) AS boundry_runs
         FROM deliveries
         JOIN players ON players.player_id = deliveries.batsman
         JOIN matches ON matches.id = deliveries.match_id
         WHERE matches.season = 2015 AND players.player_name = 'MS Dhoni'
         AND deliveries.batsman_runs IN(4,6);`
         executeQry(conn, dhoniBoundryRuns2015)

         const bumrahAllWickets2016 = `SELECT players.player_name AS bowler,
         COUNT(deliveries.player_dismissed) AS total_wickets
        FROM deliveries
        JOIN players ON players.player_id = deliveries.bowler
        JOIN matches ON matches.id = deliveries.match_id
        WHERE matches.season = 2017 AND players.player_name = 'JJ Bumrah'
        AND deliveries.dismissal_kind NOT IN('retired hurt', 'run out', 'obstructing the field');`
        executeQry(conn, bumrahAllWickets2016)

        const bumrahAllWicketsByBowled2017 = `SELECT players.player_name AS bowler,
        COUNT(deliveries.player_dismissed) AS total_bowled
       FROM deliveries
       JOIN players ON players.player_id = deliveries.bowler
       JOIN matches ON matches.id = deliveries.match_id
       WHERE matches.season = 2016 AND players.player_name = 'JJ Bumrah'
       AND deliveries.dismissal_kind = 'bowled';`
       executeQry(conn, bumrahAllWicketsByBowled2017)


        }
        conn.end()
    })
} catch(err) {
    console.log(err)
}
