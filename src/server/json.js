const fs = require('fs')

/* 
 get json data for parameter path */
 function readJson(jsonFilePath) {
    return fs.promises.readFile(jsonFilePath, 'utf-8')
}

/* 
 write json data into path */
function writeJson(jsonFile, jsonPath){
    fs.promises.writeFile(jsonPath, JSON.stringify(jsonFile), 'utf-8');
}

module.exports = {readJson, writeJson}