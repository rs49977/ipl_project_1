const path = require('path')
const iplDb = require('./db.js')
const json = require('./json.js')


/* 
main function to get all ipl data */
async function main() {
    const matchesJsonPath = path.resolve(__dirname, '../data/matches.json')
    const deliveriesJsonPath = path.resolve(__dirname, '../data/deliveries.json')
    const jsonDataPath = path.resolve(__dirname, '../public/db/')

    try{
        const matchesDataArr =  JSON.parse(await json.readJson(matchesJsonPath))
        const deliveriesDataArr = JSON.parse(await json.readJson(deliveriesJsonPath))
        // console.log(deliveriesDataArr.length)

        const iplAllTeamsObj = iplDb.getAllteams(matchesDataArr)
        writeJson(iplAllTeamsObj, path.join(jsonDataPath, 'iplAllTeamsObj.json'))

        const iplAllPlayersObj = iplDb.getAllPlayers(deliveriesDataArr)
        writeJson(iplAllPlayersObj, path.join(jsonDataPath, 'iplAllPlayersObj.json'))

        const iplAllUmpiresObj = iplDb.getAllUmpires(matchesDataArr)
        writeJson(iplAllUmpiresObj, path.join(jsonDataPath, 'iplAllUmpiresObj.json'))

        const iplAllStadiumsObj = iplDb.getAllStadiums(matchesDataArr)
        writeJson(iplAllStadiumsObj, path.join(jsonDataPath, 'iplAllStadiumsObj.json'))
        // console.log(iplAllStadiumsObj)

    } catch (err) {
        console.log(err)
    }
}

main()