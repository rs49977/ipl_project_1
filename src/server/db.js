/* 
 */
function getUniqueItem(itemsObj, itemsArr) {
    itemsArr.forEach(item => {
        if(!itemsObj.hasOwnProperty(item) && item != '') itemsObj[item] = Object.keys(itemsObj).length + 1
    })
}

/* 
get all team name of all ipl season */
function getAllteams(matchesArr) {
    return matchesArr.reduce((teams, match) => {
        let teamsArr = [match.team1, match.team2]
        getUniqueItem(teams, teamsArr)
        return teams
    },{})
}

/* 
get all player name of all ipl season */
function getAllPlayers(deliveriesArr) {
    return deliveriesArr.reduce((players, delivery) => {
        let playersArr = [delivery.batsman, delivery.non_striker, delivery.bowler, delivery.fielder]
        getUniqueItem(players, playersArr)
        return players
    },{})
}

/* 
get all umpires in ipl all seasons */
function getAllUmpires(matchesArr) {
    return matchesArr.reduce((umpires, match) => {
        let umpiresArr = [match.umpire1, match.umpire2, match.umpire3 ]
        getUniqueItem(umpires, umpiresArr)
        return umpires
    },{})
}

/* 
get all season satadium */
function getAllStadiums(matches) {
    return matches.reduce((stadiums, data, i) => {
        if (!(data.venue in stadiums)) {
            stadiums[data.venue] = {}
            stadiums[data.venue][Object.keys(stadiums).length] = data.city
        }
        return stadiums
    }, {})

}



module.exports = {getAllteams, getAllPlayers, getAllUmpires, getAllStadiums}