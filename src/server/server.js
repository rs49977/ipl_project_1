const http = require('http')
const path = require('path')
const fs = require('fs')
const portNum = 4000

// server to connect provide data to client side
const server = http.createServer((request, response) => {
    const requestUrl = request.url;
    let urlExtension = requestUrl.split('.').slice(-1)[0]

    switch(urlExtension){
        case '/' : 
            const filePath = path.resolve(__dirname,'../client/index.html')
            response.writeHead(200, { 'Content-Type' : 'text/html'})
            fs.readFile(filePath,(err,data)=>{
                if(err) {
                    response.writeHead(404)
                    response.write('file not found')
                } else {
                    response.write(data)
                }
                response.end()
                })
            break
        case 'js':
            const jsFilePath = path.resolve(__dirname,'../client/plots.js')
            // console.log(jsFilePath)
            response.writeHead(200, { 'Content-Type' : 'application/javascript'})
            fs.readFile(jsFilePath,(err,data)=>{
                if(err) {
                    response.writeHead(404)
                    response.write('file not found')
                } else {
                    response.write(data)
                }
                response.end()
                })
            break
        case 'json':
            const jsonFilePath = path.join(__dirname,'../public/output/',requestUrl)
            // console.log(jsonFilePath)
            response.writeHead(200, { 'Content-Type' : 'application/json'})
            fs.readFile(jsonFilePath,(err,data)=>{
                if(err) {
                    response.writeHead(404)
                    response.write('file not found')
                } else {
                    response.write(data)
                }
                response.end()
                })
            break
    }
})

server.listen(4000,(err) => {
    if(err) console.log(err)
    else console.log(`server listining port :${portNum}`)
})
