const mysql = require('mysql')
const dotenv = require('dotenv')
const path = require('path')
const json = require('./json.js')

const iplTablesDataPath = path.resolve(__dirname, '../public/db/')
const iplMatchesDataPath = path.resolve(__dirname,'../data/matches.json')
const iplDeliveriesDataPath = path.resolve(__dirname,'../data/deliveries.json')

const result = dotenv.config()
if (result.error) console.log(result.parsed)
   
const conn = mysql.createConnection({
    host: process.env.DATABASE_HOST, 
    user: process.env.DATABASE_USER, 
    password: process.env.DATABASE_PASS
})


function insertIntoTable(sqlServer, sqlQuery, data) {
    return sqlServer.query(sqlQuery, [data], (err, result) => {
        if(err) throw err
        else {
            console.log('successfully inserted')
            return result
        }
    })
}

function executeQry(sqlServer, sqlQuery) {
    return sqlServer.query(sqlQuery, (err, result) => {
        if(err) throw err
        else {
            console.log('executed successfully !!')
            return result
        }
    })
}


async function createIplDatabase() {
    try{
        const iplAllMatchesArr = JSON.parse(await json.readJson(iplMatchesDataPath))
        const iplAllDeliveriesArr = JSON.parse(await json.readJson(iplDeliveriesDataPath))

        const iplAllTeamsObj = JSON.parse(await json.readJson(path.join(iplTablesDataPath,'iplAllTeamsObj.json')))
        const iplAllPlayersObj = JSON.parse(await json.readJson(path.join(iplTablesDataPath,'iplAllPlayersObj.json')))
        const iplAllUmpiresObj = JSON.parse(await json.readJson(path.join(iplTablesDataPath, 'iplAllUmpiresObj.json')))
        const iplAllStadiumsObj = JSON.parse(await json.readJson(path.join(iplTablesDataPath, 'iplAllStadiumsObj.json')))

        try {
            conn.connect(err => {
                if(err) throw err
                else {
                    console.log('connected!')
                    const createIplDbQry = 'CREATE DATABASE IF NOT EXISTS ipl'
                    const check1 = executeQry(conn, createIplDbQry)

                    const useIplDbQry = 'USE ipl'
                    const result1 = executeQry(conn, useIplDbQry)


                    const createTeamsTbQry = `CREATE TABLE IF NOT EXISTS teams 
                    (team_id INT AUTO_INCREMENT PRIMARY KEY, team_name VARCHAR(50))`
                    // const check4 = executeQry(conn, createTeamsTbQry)
                    // console.log(check4)
            
                    const insertTeamsQry = `INSERT INTO teams (team_id, team_name) VALUES ?`
                    const values4 = Object.keys(iplAllTeamsObj).map(team => [iplAllTeamsObj[team], team])
                    // const result4 = insertIntoTable(conn, insertTeamsQry, values4)
                    // console.log(result2)


                    const createPlayersTbQry = `CREATE TABLE IF NOT EXISTS players 
                    (player_id INT AUTO_INCREMENT PRIMARY KEY, player_name VARCHAR(50))`
                    // const check2 = executeQry(conn, createPlayersTbQry)
                    // console.log(check2)
            
                    const insertPlayersQry = `INSERT INTO players (player_id, player_name) VALUES ?`
                    const values1 = Object.keys(iplAllPlayersObj).map(player => [iplAllPlayersObj[player], player])
                    // const result2 = insertIntoTable(conn, insertPlayersQry, values1)
                    // console.log(result2)



                    const createUmpiresTbQry = `CREATE TABLE IF NOT EXISTS umpires 
                    (umpire_id INT AUTO_INCREMENT PRIMARY KEY, umpire_name VARCHAR(50))`
                    // const check3 = executeQry(conn, createUmpiresTbQry)

                    const insertUmpiresQry = `INSERT INTO umpires (umpire_id, umpire_name) VALUES ?`
                    const values2 = Object.keys(iplAllUmpiresObj).map(umpire => [iplAllUmpiresObj[umpire], umpire])
                    // const result3 = insertIntoTable(conn, insertUmpiresQry, values2)


                    const createStadiumsTbQry = `CREATE TABLE IF NOT EXISTS stadiums 
                    (venue_id INT AUTO_INCREMENT PRIMARY KEY, venue VARCHAR(200), city VARCHAR(100))`
                    // const check4 = executeQry(conn, createStadiumsTbQry)

                    const insertStadiumsQry = `INSERT INTO stadiums (venue_id, venue, city) VALUES ?`
                    const values3 = Object.keys(iplAllStadiumsObj).map(venue => {
                        const venue_id = Object.keys(iplAllStadiumsObj[venue])[0]
                        const city = iplAllStadiumsObj[venue][venue_id]
                        return [venue_id, venue, city]
                    })
                    // console.log(values3)
                    // const result4 = insertIntoTable(conn, insertStadiumsQry, values3)  

                    const createMatchesTbQry = `CREATE TABLE IF NOT EXISTS matches 
                    (
                        id INT AUTO_INCREMENT PRIMARY KEY, season INT, date VARCHAR(12), venue INT, team1 INT,
                        team2 INT, toss_winner INT, toss_decision  VARCHAR(10), result  VARCHAR(10), 
                        dl_applied  VARCHAR(10), winner INT, win_by_runs INT, win_by_wickets INT, 
                        player_of_match INT, umpire1 INT, umpire2 INT, umpire3 INT
                    )`
                    // const check5 = executeQry(conn, createMatchesTbQry)

                    const insertMatchesQry = `INSERT INTO matches 
                    (
                        id, season, date, venue, team1, team2, toss_winner, toss_decision, result, 
                        dl_applied, winner, win_by_runs, win_by_wickets, player_of_match, umpire1, umpire2, umpire3
                    ) VALUES ?`

                    const values5 = iplAllMatchesArr.map(match => {
                        return [match.id, match.season, match.date, Object.keys(iplAllStadiumsObj[match.venue])[0], iplAllTeamsObj[match.team1],
                        iplAllTeamsObj[match.team2], iplAllTeamsObj[match.toss_winner], match.toss_decision, match.result,
                        match.dl_applied, iplAllTeamsObj[match.winner], match.win_by_runs, match.win_by_wickets, iplAllPlayersObj[match.player_of_match],
                        iplAllUmpiresObj[match.umpire1], iplAllUmpiresObj[match.umpire2], iplAllUmpiresObj[match.umpire3]] 
                    })
                    // console.log(values5)
                    // const result5 = insertIntoTable(conn, insertMatchesQry, values5)  

                    const createDeliveriesTbQry = `CREATE TABLE IF NOT EXISTS deliveries 
                    (
                        id INT AUTO_INCREMENT PRIMARY KEY, match_id INT, inning INT, batting_team INT, bowling_team INT, 
                        over_no  TINYINT, ball  TINYINT, batsman INT, non_striker INT, bowler INT, is_super_over VARCHAR(100), 
                        wide_runs TINYINT, bye_runs TINYINT, legbye_runs TINYINT, noball_runs TINYINT, penalty_runs TINYINT, 
                        batsman_runs TINYINT, extra_runs TINYINT, total_runs TINYINT, player_dismissed INT, 
                        dismissal_kind VARCHAR(50), fielder INT
                    )`
                    // const check6 = executeQry(conn, createDeliveriesTbQry)

                    const insertDeliveriesQry = `INSERT INTO deliveries 
                    (
                        match_id, inning, batting_team, bowling_team, over_no, ball, batsman, non_striker, bowler, 
                        is_super_over, wide_runs, bye_runs, legbye_runs, noball_runs, penalty_runs, batsman_runs, 
                        extra_runs, total_runs, player_dismissed, dismissal_kind, fielder
                    ) VALUES ?`

                    const values6 = iplAllDeliveriesArr.map(delivery => {
                        return [delivery.match_id, delivery.inning, iplAllTeamsObj[delivery.batting_team],  iplAllTeamsObj[delivery.bowling_team], delivery.over, delivery.ball, 
                        iplAllPlayersObj[delivery.batsman], iplAllPlayersObj[delivery.non_striker], iplAllPlayersObj[delivery.bowler], delivery.is_super_over, delivery.wide_runs, 
                        delivery.bye_runs, delivery.legbye_runs, delivery.noball_runs, delivery.penalty_runs, delivery.batsman_runs, delivery.extra_runs, delivery.total_runs, 
                        iplAllPlayersObj[delivery.player_dismissed], delivery.dismissal_kind, iplAllPlayersObj[delivery.fielder]]
                    })
                    // console.log([values6[1]])
                    // const result6 = insertIntoTable(conn, insertDeliveriesQry, values6)
                }
                conn.end()
                })
            } catch(err) {
                console.log(err)
            }
    
    } catch (err) {
        throw err;
    }
    
}

createIplDatabase()







