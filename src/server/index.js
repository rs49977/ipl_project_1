const path = require('path')
const fs = require('fs')
const ipl = require('./ipl')


// function to get data from json file
function readJsonFile(jsonFile){
    const filePath = path.resolve(__dirname,jsonFile)
    const jsonData = fs.readFileSync(filePath,(err) => {
        if(err) console.log(err);
    })
    return JSON.parse(jsonData);
}

// function to write data into json file
function writeJsonFile(jsonData,jsonFile){
    const filePath = path.resolve(__dirname,jsonFile)
    fs.writeFile(filePath,JSON.stringify(jsonData),(err) =>{
        console.log(err);
    })
}

function main() {
    const matchesData = readJsonFile('../data/matches.json')
    const deliveriesData = readJsonFile('../data/deliveries.json')
    
    const allSeasonMatches = ipl.allSeasonMatches(matchesData)
    writeJsonFile(allSeasonMatches,'../public/output/allSeasonMatches.json')

    const allSeasonWinCount = ipl.allSeasonWins(matchesData)
    writeJsonFile(allSeasonWinCount,'../public/output/allSeasonWinCount.json')

    const topTenBowler2015 = ipl.topBowlersByEconomy(matchesData,deliveriesData)
    writeJsonFile(topTenBowler2015,'../public/output/topTenBowler2015.json')

    const extraRunsPerTeam2016 = ipl.extraRunsPerTeam(matchesData,deliveriesData)
    writeJsonFile(extraRunsPerTeam2016,'../public/output/extraRunsPerTeam2016.json')
}
main();
