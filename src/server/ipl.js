
// return all season played matches count
function allSeasonMatches(matchesData){
    if(matchesData == null) return
    return matchesData.reduce((seasons,match) => {
        if(seasons.hasOwnProperty(match.season)) seasons[match.season] += 1
        else seasons[match.season] = 1
        return seasons
    },{})
}

// return all season per, teams with win count property
function allSeasonWins (matchesData){
    if(matchesData == null) return
    return matchesData.reduce((seasons,match) => {
        if(seasons.hasOwnProperty(match.season)){
            if(seasons[match.season].hasOwnProperty(match.winner)) seasons[match.season][match.winner] += 1
            else seasons[match.season][match.winner] = 1
        } else {
            seasons[match.season] = {}
            seasons[match.season][match.winner] = 1
        }
        return seasons
    },{})
}

// return top ten economy bowler of 2015
function topBowlersByEconomy(matchesData,deliveriesData,season='2015'){
    if(matchesData == null || deliveriesData == null) return
    const matchId = matchesData.filter(row => row.season == season).map(match => match.id)
    const matchedMatches = deliveriesData.filter((data) => matchId.indexOf(data.match_id) !== -1)
    const bowlersTotalRunAndBall = matchedMatches.reduce((bowlers,ball) => {
        if(bowlers.hasOwnProperty(ball.bowler)) {
            bowlers[ball.bowler]['run'] += +ball.total_runs
            bowlers[ball.bowler]['ball'] += 1
        } else {
            bowlers[ball.bowler] = {}
            bowlers[ball.bowler]['run'] = +ball.total_runs
            bowlers[ball.bowler]['ball'] = 1
        }
        return bowlers
    },{})
    const topTenBolwers = Object.keys(bowlersTotalRunAndBall).map((bowler) =>{
        let totalRun = bowlersTotalRunAndBall[bowler]['run']
        let totalOver =  bowlersTotalRunAndBall[bowler]['ball']/6
        return [bowler,(totalRun/totalOver).toFixed(2)]
    }).sort((a,b) => a[1]-b[1]).slice(0,10).reduce((bowlers,data) =>{
        bowlers[data[0]] = data[1]
        return bowlers
    },{})
    return topTenBolwers
}

// return per teams given extra runs 2016
function extraRunsPerTeam (matchesData,deliveriesData,season='2016') {
    if(matchesData == null || deliveriesData == null) return
    const matchId = matchesData.filter(row => row.season == season).map(match => match.id)
    const matchedMatches = deliveriesData.filter((data) => matchId.indexOf(data.match_id) !== -1)
    const extraRunsPerTeam = matchedMatches.reduce((teams,ball) => {
        if (teams.hasOwnProperty(ball.bowling_team)) teams[ball.bowling_team] += +ball.extra_runs
        else teams[ball.bowling_team] = +ball.extra_runs
        return teams
    },{})
    return extraRunsPerTeam
}

module.exports = {allSeasonMatches, allSeasonWins, topBowlersByEconomy, extraRunsPerTeam};