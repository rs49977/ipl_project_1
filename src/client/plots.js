
fetch('allSeasonMatches.json')
.then(response => {
    // console.log(response)
    return response.json()})
.then(data => {
        console.log(data)
        Highcharts.chart('allSeasonMatches', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Number of matches played in ipl all seasons'
            },
            
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Number of matches'
                }
            },
            legend: {
                enabled: true
            },
            tooltip: {
                pointFormat: 'matches played : <b>{point.y}</b>'
            },
            series: [{
                name: 'matches',
                data: Object.keys(data).map((key) =>{
                    return [key,data[key]]
                }),
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
                      
})

// fetch('extraRunsPerTeam2016.json')
// .then(response => response.json())
// .then(data =>{
//         console.log(data)
//         Highcharts.chart('extraRunsPerTeam2016', {
//             chart: {
//                 type: 'column'
//             },
//             title: {
//                 text: 'Extra runs thrown by all team in season 2016'
//             },
//             xAxis: {
//                 type: 'category',
//                 labels: {
//                     rotation: -45,
//                     style: {
//                         fontSize: '13px',
//                         fontFamily: 'Verdana, sans-serif'
//                     }
//                 }
//             },
//             yAxis: {
//                 min: 0,
//                 title: {
//                     text: 'Extra runs'
//                 }
//             },
//             legend: {
//                 enabled: true
//             },
//             tooltip: {
//                 pointFormat: 'thrown extra runs : <b>{point.y}</b>'
//             },
//             series: [{
//                 name: 'runs',
//                 data: Object.keys(data).map((key) =>{
//                     return [key,data[key]]
//                 }),
//                 dataLabels: {
//                     enabled: true,
//                     rotation: -90,
//                     color: '#FFFFFF',
//                     align: 'right',
//                     format: '{point.y}', // one decimal
//                     y: 10, // 10 pixels down from the top
//                     style: {
//                         fontSize: '13px',
//                         fontFamily: 'Verdana, sans-serif'
//                     }
//                 }
//             }]
//         });
                      
// })

fetch('extraRunsPerTeam2016.json')
.then(response => response.json())
.then(data =>{
        // console.log(data)
        const chart = Highcharts.chart('extraRunsPerTeam2016', {
            title: {
                text: 'Extra runs thrown by all team in season 2016'
            },
            subtitle: {
                text: 'Plain'
            },
            xAxis: {
                categories: Object.keys(data)
            },
            yAxis: {
                     min: 0,
                    title: { text: 'Extra runs' }
            },
            tooltip: {
                  pointFormat: 'thrown extra runs : <b>{point.y}</b>'
             },
            series: [{
                // title = 'hh',
                type: 'column',
                colorByPoint: true,
                data: Object.values(data),
                showInLegend: true,
                
            }]
        });
        
        document.getElementById('plain').addEventListener('click', () => {
            chart.update({
                chart: {
                    inverted: false,
                    polar: false
                },
                subtitle: {
                    text: 'Plain'
                }
            });
        });
        
        document.getElementById('inverted').addEventListener('click', () => {
            chart.update({
                chart: {
                    inverted: true,
                    polar: false
                },
                subtitle: {
                    text: 'Inverted'
                }
            });
        });
                       
})

fetch('topTenBowler2015.json')
.then(response => response.json())
.then(data =>{
        console.log(data)
        Highcharts.chart('topTenBowler2015', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Top 10 economy bowler of 2015'
            },
            // subtitle: {
            //     text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
            // },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Economy of bowlers'
                }
            },
            legend: {
                enabled: true
            },
            tooltip: {
                pointFormat: 'gives run : <b>{point.y}</b> per over'
            },
            series: [{
                name: 'Season 2015',
                colorByPoint: true,
                data: Object.keys(data).map((key) =>{
                    return [key,parseFloat(data[key])]
                }),
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y:.1f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
                      
})


fetch('allSeasonWinCount.json')
.then(response => response.json())
.then(data =>{
    const allSeason = Object.keys(data)
    let team = []
    let h = {}
    allSeason.forEach((season) => {
        for(let i in data[season]){
            if(i == "") continue
            if (team.indexOf(i) == -1) team.push(i)
        }
    })

    allSeason.forEach((season) => {
        team.forEach((teamname) =>{
        if(Object.keys(data[season]).includes(teamname)){
            if(teamname in h){
                h[teamname]['data'].push(data[season][teamname])
            } else {
                h[teamname] = { }
                h[teamname]['data'] = []
                h[teamname]['data'].push(data[season][teamname])
            }
        } else {
            if(teamname in h){
                h[teamname]['data'].push(null)
            } else {
                h[teamname] = { }
                h[teamname]['data'] = []
                h[teamname]['data'].push(null)
            }

        }
        })
    })
 

    // console.log(result)
    Highcharts.chart('allSeasonWinCount', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Ipl all Season all team wining count'
        },
        // subtitle: {
        //     text: 'Source: WorldClimate.com'
        // },
        xAxis: {
            categories: allSeason,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of wining matches'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} matches</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: team.map((teamname) =>{
            return {name:teamname,data:h[teamname]['data']}
        })
    });
         
  
})
